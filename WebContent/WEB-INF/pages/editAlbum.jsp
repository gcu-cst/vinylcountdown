<body>
	<div class="loginContainer">
		<h1>Album Edit</h1>
		
		<form action="/vinylCountdown/albums"><input class="btn btn-dark" type="submit" value="Back" /></form>
		
		<form class="inputForm" method="POST" action="/vinylCountdown/save">
			<div class="form-group row justify-content-lg-center" style="width:100%;">
				<label for="albumName" class="col-sm-2 col-lg-2 col-form-label">Album Name: </label>
				<div class="col-sm-9 col-lg-4">
				 	<input type="text" name="albumName" class="form-control" id="albumName" readonly value="${album.getAlbumName()}">
				</div>
			</div>
			<div class="form-group row justify-content-lg-center" style="width:100%;">
				<label for="artistName" class="col-sm-2 col-lg-2 col-form-label">Artist Name: </label>
				<div class="col-sm-9 col-lg-4">
					<input type="text" name="artistName" class="form-control" id="lastName" readonly value="${album.getArtistName()}">
				</div>
			</div>
			<div class="form-group row justify-content-lg-center" style="width:100%;">
				<label for="genre" class="col-sm-2 col-lg-2 col-form-label">Genre: </label>
				<div class="col-sm-9 col-lg-4">
				 	<input type="text" name="genre" class="form-control" id="genre" value="${album.getGenre()}">
				</div>
			</div>
			<div class="form-group row justify-content-lg-center" style="width:100%;">
				<label for="releaseDate" class="col-sm-2 col-lg-2 col-form-label">Release Date: </label>
				<div class="col-sm-9 col-lg-4">
					<input type="text" name="releaseDate" class="form-control" id="releaseDate" readonly value="${album.getReleaseDate()}">
				</div>
			</div>
			<div class="form-group row justify-content-lg-center" style="width:100%;">
				<label for="tracks" class="col-sm-2 col-lg-2 col-form-label">Tracks: </label>
				<div class="col-sm-9 col-lg-4">
				 	<input type="text" name="tracks" class="form-control" id="tracks" readonly value="${album.getTracks()}"/>
				</div>
			</div>
			<div class="form-group row justify-content-lg-center" style="width:100%;">
				<label for="length" class="col-sm-2 col-lg-2 col-form-label">Length: </label>
				<div class="col-sm-9 col-lg-4">
					<input type="text" name="length" class="form-control" id="length" readonly value="${album.getLength()}">
				</div>
			</div>
			<input class="btn btn-dark" type="submit" value="Save"/>
		</form>
	</div>
</body>