<div class="loginContainer">
	<h1>Vinyl Countdown</h1>

	<form class="inputForm" method="POST" action="/vinylCountdown/login">
		<div class="form-group row justify-content-lg-center" style="width:100%;">
			<label for="userName" class="col-sm-2 col-lg-1 col-form-label">Username: </label>
			<div class="col-sm-10 col-lg-4">
			 	<input type="text" name="userName" class="form-control" id="userName">
			</div>
		</div>
		<div class="form-group row justify-content-lg-center" style="width:100%;">
			<label for="password" class="col-sm-2 col-lg-1  col-form-label">Password: </label>
			<div class="col-sm-10 col-lg-4">
				<input type="password" name="password" class="form-control" id="password">
			</div>
		</div>
		<input class="btn btn-dark" type="submit" value="Login"/>
	</form>
</div>
