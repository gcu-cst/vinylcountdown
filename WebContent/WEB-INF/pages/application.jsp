<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
  
<body>
	<div class="loginContainer">
		<h1>Album List</h1> 
		
		<div style = "text-align: center">
			<form:form method="GET" action="/vinylCountdown/add">
		    	<input type="submit" class="btn btn-dark" value="Add New Album" />
		    </form:form> 
	    </div>
	    
	
		<table class="table table-responsive-sm table-hover"> 
			<thead class="thead-dark"> 
				<tr>
					<th scope="col">Album Name</th> 
					<th scope="col">Artist Name</th> 
					<th scope="col">Genre</th> 
					<th scope="col">Release Date</th>
					<th scope="col">Length</th>
					<th scope="col">Tracks</th>
					<th scope="col"></th>
					<th scope="col"></th>
				</tr>
			</thead> 
			<tbody>
				<c:forEach items="${albums}" var="album">
					<tr> 
						<th scope="row">${album.getAlbumName()}</th> 
						<td>${album.getArtistName()}</td> 
						<td>${album.getGenre()}</td> 
						<td>${album.getReleaseDate()}</td> 
						<td>${album.getLength()}</td> 
						<td>${album.getTracks()}</td> 
						<td>
							<a class="btn btn-default btn-sm" href="/vinylCountdown/edit/${album.getAlbumName()}" style="color: black;">
	          					<span><i class="fa fa-pencil"></i></span>
	        				</a>
	        			</td>
	        			<td>
							<a class="btn btn-default btn-sm" href="/vinylCountdown/delete/${album.getAlbumName()}" style="color: black;">
	          					<span><i class="fa fa-trash-o"></i></span>
	        				</a>
	        			</td>
					</tr> 
				</c:forEach>
			</tbody>
		</table>  
	</div>
</body> 