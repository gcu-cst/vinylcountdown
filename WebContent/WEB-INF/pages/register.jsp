=<div class="loginContainer">
	<h1>Registration</h1>
	
	<form class="inputForm" method="POST" action="/vinylCountdown/register">
		<div class="form-group row justify-content-lg-center" style="width:100%;">
			<label for="firstName" class="col-sm-3 col-lg-2 col-form-label">First Name: </label>
			<div class="col-sm-9 col-lg-4">
			 	<input type="text" name="firstName" class="form-control" id="firstName">
			</div>
		</div>
		<div class="form-group row justify-content-lg-center" style="width:100%;">
			<label for="lastName" class="col-sm-3 col-lg-2 col-form-label">Last Name: </label>
			<div class="col-sm-9 col-lg-4">
				<input type="text" name="lastName" class="form-control" id="lastName">
			</div>
		</div>
		<div class="form-group row justify-content-lg-center" style="width:100%;">
			<label for="userName" class="col-sm-3 col-lg-2 col-form-label">Username: </label>
			<div class="col-sm-9 col-lg-4">
			 	<input type="text" name="userName" class="form-control" id="userName">
			</div>
		</div>
		<div class="form-group row justify-content-lg-center" style="width:100%;">
			<label for="password" class="col-sm-3 col-lg-2 col-form-label">Password: </label>
			<div class="col-sm-9 col-lg-4">
				<input type="text" name="password" class="form-control" id="password">
			</div>
		</div>
		<div class="form-group row justify-content-lg-center" style="width:100%;">
			<label for="email" class="col-sm-3 col-lg-2 col-form-label">Email: </label>
			<div class="col-sm-9 col-lg-4">
			 	<input type="text" name="email" class="form-control" id="email">
			</div>
		</div>
		<div class="form-group row justify-content-lg-center" style="width:100%;">
			<label for="phoneNumber" class="col-sm-3 col-lg-2 col-form-label">Phone Number: </label>
			<div class="col-sm-9 col-lg-4">
				<input type="text" name="phoneNumber" class="form-control" id="phoneNumber">
			</div>
		</div>
		<input class="btn btn-dark" type="submit" value="Register"/>
	</form>
</div>

