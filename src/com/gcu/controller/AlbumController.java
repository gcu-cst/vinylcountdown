package com.gcu.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gcu.model.Album;
import com.gcu.service.AlbumService;

/**
 * 
 * @author James Suderman & Raymond Lawson
 * Album Controller
 * This controller handles all requests for albums
 *
 */

@Controller
public class AlbumController {
	
	private AlbumService albumService;
	
	/**
	 * This is the main application
	 * @param model is forwarded over on the redirect from the login
	 * @return the main application
	 */
	@RequestMapping(path="/albums", method=RequestMethod.GET)
	public ModelAndView displayAlbums(ModelMap model) {
		ModelAndView appMav = new ModelAndView();

		appMav.setViewName("application");
		appMav.addObject("albums", albumService.getAlbums());

		return appMav;
	}
	
	/**
	 * The edit path takes the name of an album
	 * @param album is passed in through the GET parameters
	 * @return the album details page for editing
	 */
	@RequestMapping(path="/edit/{album}", method=RequestMethod.GET)
	public ModelAndView displayEditForm(@PathVariable(value="album") String album) {
		ModelAndView editMav = new ModelAndView();
		Album albumToEdit = albumService.getAlbumByName(album);
		
		editMav.setViewName("editAlbum");
		editMav.addObject("album", albumToEdit);

		return editMav;
	}
	
	@RequestMapping(path="/save", method=RequestMethod.POST)
	public ModelAndView save(@ModelAttribute Album album) {
		ModelAndView appMav = new ModelAndView();
		albumService.saveAlbum(album);
		
		appMav.setViewName("application");
		appMav.addObject("albums", albumService.getAlbums());
		
		return appMav;
	}
	
	@RequestMapping(path ="/delete/{album}", method=RequestMethod.GET)
	public ModelAndView delete(@PathVariable(value="album") String album) {
		ModelAndView newappMAV = new ModelAndView();
		albumService.deleteAlbum(album);
		
		newappMAV.setViewName("application");
		newappMAV.addObject("albums", albumService.getAlbums());
		
		return newappMAV;
	}
	
	@RequestMapping(path = "/add", method=RequestMethod.GET)
	public ModelAndView add() {
		ModelAndView addMAV = new ModelAndView();
		addMAV.setViewName("newAlbum");
		addMAV.addObject("album", new Album());
		return addMAV;
	}
	
	@RequestMapping(path="/saveAlbum", method=RequestMethod.POST)
	public ModelAndView saveAlbum(@Valid @ModelAttribute Album album, BindingResult result) {
		if(result.hasErrors()) {
			ModelAndView addMAV = new ModelAndView();
			addMAV.setViewName("newAlbum");
			addMAV.addObject("album", new Album());
			return addMAV;
		}
		
		ModelAndView appMav = new ModelAndView();
		albumService.addAlbum(album);
		appMav.setViewName("application");
		appMav.addObject("albums", albumService.getAlbums());
		
		return appMav;
	}
	
	@Autowired
	public void setAlbumService(AlbumService albumService) {
		this.albumService = albumService;
	}

}
