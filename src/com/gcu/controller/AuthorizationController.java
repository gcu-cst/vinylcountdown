package com.gcu.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gcu.model.User;
import com.gcu.service.AuthorizationService;

/**
 * 
 * @author James Suderman & Raymond Lawson
 * Authorization Controller
 * This controller handles all requests for logging in and registering
 *
 */

@Controller
public class AuthorizationController {
	
	private AuthorizationService authorizationService;

	/**
	 * Landing page is the login view
	 * @return login view
	 */
	@RequestMapping(path="/", method=RequestMethod.GET)
	public ModelAndView displayLoginForm() {
		return new ModelAndView("login", "user", new User());
	}
	
	@RequestMapping(path="/registration", method=RequestMethod.GET)
	public ModelAndView displayRegisterForm() {
		return new ModelAndView("register", "user", new User());
	}
	
	@RequestMapping(path="/login", method=RequestMethod.POST)
	public ModelAndView login(@ModelAttribute User user) {
		User appUser = null;
		
		try {
			appUser = authorizationService.authorizeUser(user);
		} catch(Exception e){
			System.out.println(e);
		}
		
		/**
		 * If user is received from authorization service send them to app
		 * If not, then place them back on login page
		 */
		if(appUser != null) {
			return new ModelAndView("redirect:/albums", "user", appUser);
			
		} else {
			return new ModelAndView("autherror", "user", new User());
		}
	}
	
	/**
	 * Simply saves user from any info given on registration page
	 * @param user
	 * @return application view
	 */
	@RequestMapping(path="/register", method=RequestMethod.POST)
	public ModelAndView register(@Valid @ModelAttribute User user, BindingResult result) {
		
		if(result.hasErrors()) {
			return new ModelAndView("register", "user", user);
		}
		
		User appUser = authorizationService.registerUser(user);
		return new ModelAndView("redirect:/albums", "user", appUser);
	}
	
	@Autowired
	public void setAuthorizationService(AuthorizationService authorizationService) {
		this.authorizationService = authorizationService;
	}
}
