package com.gcu.data;

import java.util.List;

import com.gcu.model.Album;

public interface AlbumDataAccessInterface {
	public List<Album> findAll();
	public Album findByAlbumName(String name);
	public boolean create(Album album);
	public boolean update(Album album);
	public boolean delete(String albumName);
}
