package com.gcu.data;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.gcu.model.User;

/**
 * 
 * @author James Suderman & Raymond Lawson
 * User Data Service
 * This service communicates with the UserDataAccessInterface
 * Each query utilizes the user data
 *
 */

public class UserDataService implements UserDataAccessInterface {
	
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<User> findAll() {
		List<User> users = new ArrayList<>();
		
		try {			
			jdbcTemplate.query("SELECT * FROM vinyl.Users", (rs, rn) -> users.add(new User(rs.getString("FIRST_NAME"), 
								rs.getString("LAST_NAME"), rs.getString("USER_NAME"), rs.getString("PASSWORD"), rs.getString("EMAIL"), rs.getString("PHONE_NUMBER"))));
		} catch (Exception e) {
			System.out.println("FAILURE");
		}
		
		return users;
	}

	@Override
	public User findByUsername(String name) {	
		try {	
			return jdbcTemplate.queryForObject("SELECT * FROM vinyl.Users WHERE USER_NAME = ?", new Object[] {name}, (rs, rn) -> new User(rs.getString("FIRST_NAME"), 
												rs.getString("LAST_NAME"), rs.getString("USER_NAME"), rs.getString("PASSWORD"), rs.getString("EMAIL"), rs.getString("PHONE_NUMBER")));
			
		} catch (Exception e) {
			System.out.println("FAILURE");
		}
		
		return null;
	}

	@Override
	public boolean create(User user) {
		try {
			jdbcTemplate.update("INSERT INTO vinyl.Users(FIRST_NAME, LAST_NAME, USER_NAME, PASSWORD, EMAIL, PHONE_NUMBER) VALUES (?, ?, ?, ?, ?, ?)",
					new Object[] {user.getFirstName(), user.getLastName(), user.getUserName(), user.getPassword(), user.getEmail(), user.getPhoneNumber()});
			return true;
		} catch (Exception e) {
			System.out.println("FAILURE");
			return false;
		}
	}

	@Override
	public boolean update(User user) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(User user) {
		// TODO Auto-generated method stub
		return false;
	}

	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
}
