package com.gcu.data;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.gcu.model.Album;

/**
 * 
 * @author James Suderman & Raymond Lawson
 * Album Data Service
 * This service communicates with the AlbumDataAccessInterface
 * Each query utilizes the album data
 *
 */

public class AlbumDataService implements AlbumDataAccessInterface {
	
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Album> findAll() {
		List<Album> albums = new ArrayList<>();
		
		try {	
			jdbcTemplate.query("SELECT * FROM vinyl.Albums", (rs, rn) -> albums.add(new Album(rs.getString("ALBUM_NAME"), 
								rs.getString("ARTIST_NAME"), rs.getString("GENRE"), rs.getDate("RELEASE_DATE").toString(), rs.getString("LENGTH"), rs.getInt("TRACKS"))));
		} catch(Exception e){
			System.out.println("Unable to access database!");
		}
		
		return albums;
	}

	@Override
	public Album findByAlbumName(String name) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM vinyl.Albums WHERE ALBUM_NAME = ?", new Object[] {name}, (rs, rn) -> new Album(rs.getString("ALBUM_NAME"), 
												rs.getString("ARTIST_NAME"), rs.getString("GENRE"), rs.getDate("RELEASE_DATE").toString(), rs.getString("LENGTH"), rs.getInt("TRACKS")));
		} catch(Exception e) {
			System.out.println(e.getLocalizedMessage());
		}
		
		return null;
	}

	@Override
	public boolean create(Album album) {
		try {
			jdbcTemplate.update("INSERT INTO vinyl.Albums (ALBUM_NAME, ARTIST_NAME, GENRE, RELEASE_DATE, TRACKS, LENGTH) VALUES (?, ?, ?, ?, ?, ?)", album.getAlbumName(), album.getArtistName(), album.getGenre(), album.getReleaseDate(), album.getTracks(), album.getLength());
			return true;
		} catch(Exception e){
			System.out.println("ADDING FAILED: " + e);
		}
		
		return false;
	}

	@Override
	public boolean update(Album album) {
		try {
			jdbcTemplate.update("UPDATE vinyl.Albums SET GENRE = ? WHERE ALBUM_NAME = ?", album.getGenre(), album.getAlbumName());
			return true;
		} catch(Exception e) {
			System.out.println("UPDATE FAILED");
			return false;
		}
	}

	@Override
	public boolean delete(String albumName) {
		try {
			jdbcTemplate.update("DELETE FROM vinyl.Albums WHERE ALBUM_NAME = ?", albumName);
			return true;
		} catch (Exception e) {
			System.out.println("REMOVAL FAILED");
			return false;
		}
	}
	
	
	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
}
