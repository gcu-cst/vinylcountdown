package com.gcu.data;

import java.util.List;

import com.gcu.model.User;

public interface UserDataAccessInterface {
	public List<User> findAll();
	public User findByUsername(String name);
	public boolean create(User user);
	public boolean update(User user);
	public boolean delete(User user);
}
