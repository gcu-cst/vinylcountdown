package com.gcu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcu.data.UserDataAccessInterface;
import com.gcu.model.User;

/**
 * 
 * @author James Suderman & Raymond Lawson
 * Authorization Service
 * This service is used to handle all authorization of users logging in and registering
 *
 */

@Service
public class AuthorizationService {

	private UserDataAccessInterface userDataService;

	/**
	 * 
	 * @param user is from the model attribute on the login page
	 * @return user if the username and password match any in persistence
	 * @return null if not
	 */
	public User authorizeUser(User user) {
		User savedUser = userDataService.findByUsername(user.getUserName());
		
		if(user.getUserName().equals(savedUser.getUserName())) {
			if(user.getPassword().equals(savedUser.getPassword())) {
				return user;
			}
		}
		
		return null ;
	}
	
	/**
	 * 
	 * @param user is from the model attribute on the register page
	 * @return user once saved upon registration
	 */
	public User registerUser(User user) {
		userDataService.create(user);
		return user;
	}
	
	@Autowired
	public void setUserDataService(UserDataAccessInterface userDataService) {
		this.userDataService = userDataService;
	}
}
