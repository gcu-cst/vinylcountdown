package com.gcu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcu.data.AlbumDataAccessInterface;
import com.gcu.model.Album;

/**
 * 
 * @author James Suderman & Raymond Lawson
 * Album Service
 * This service is used to handle all the functions of interacting with albums
 *
 */

@Service
public class AlbumService {
	private AlbumDataAccessInterface albumDataService;
	
	public List<Album> getAlbums(){
		
		List<Album> albumList = new ArrayList<>();
		
		try{
			albumList = albumDataService.findAll();
		} catch(Exception e){
			System.out.println(e.getLocalizedMessage());
		}
		
		return albumList;
	}
	
	public Album getAlbumByName(String albumName) {
		return albumDataService.findByAlbumName(albumName);
	}
	
	/**
	 * saveAlbum only allows for chaging of the genre
	 * @param album is passed in through GET parameter
	 */
	public void saveAlbum(Album album) {
		albumDataService.update(album);
	}
	
	public void deleteAlbum(String albumName) {
		albumDataService.delete(albumName);
	}
	
	public void addAlbum(Album album) {
		albumDataService.create(album);
	}
	
	@Autowired
	public void setAlbumDataService(AlbumDataAccessInterface albumDataService) {
		this.albumDataService = albumDataService;
	}

}
