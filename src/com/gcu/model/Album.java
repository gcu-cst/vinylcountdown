package com.gcu.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class Album {
	@NotNull
	private String albumName;
	@NotNull
	private String artistName; 
	@NotNull
	private String genre; 
	@NotNull
	private String releaseDate; 
	@NotNull
	private String length;
	@NotNull
	@Positive(message="This field requires a number")
	private int tracks;
	
	public Album() {}
	
	public Album(String albumName, String artistName, String genre, String releaseDate, String length, int tracks){
		super();
		this.albumName = albumName;
		this.artistName = artistName;
		this.genre = genre;
		this.releaseDate = releaseDate;
		this.length = length;
		this.tracks = tracks;
	}

	public String getAlbumName() {return albumName;}
	public void setAlbumName(String albumName) {this.albumName = albumName;}
	public String getArtistName() {return artistName;}
	public void setArtistName(String artistName) {this.artistName = artistName;}
	public String getGenre() {return genre;}
	public void setGenre(String genre) {this.genre = genre;}
	public String getReleaseDate() {return releaseDate;}
	public void setReleaseDate(String releaseDate) {this.releaseDate = releaseDate;}
	public String getLength() {return length;}
	public void setLength(String length) {this.length = length;}
	public int getTracks() {return tracks;}
	public void setTracks(int tracks) {this.tracks = tracks;}
}
