package com.gcu.model;

import javax.validation.constraints.NotNull;

public class User {

	@NotNull(message="First Name cannot be empty")
	private String firstName;
	@NotNull(message="Last Name cannot be empty")
	private String lastName;
	@NotNull(message="Username cannot be empty")
	private String userName;
	@NotNull(message="Password cannot be empty")
	private String password;
	private String email;
	private String phoneNumber;
	
	public User() {}
	
	public User(String firstName, String lastName, String userName, String password, String email, String phoneNumber) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}

	public String getFirstName() {return firstName;}
	public void setFirstName(String firstName) {this.firstName = firstName;}
	public String getLastName() {return lastName;}
	public void setLastName(String lastName) {this.lastName = lastName;}
	public String getUserName() {return userName;}
	public void setUserName(String userName) {this.userName = userName;}
	public String getPassword() {return password;}
	public void setPassword(String password) {this.password = password;}
	public String getEmail() {return email;}
	public void setEmail(String email) {this.email = email;}
	public String getPhoneNumber() {return phoneNumber;}
	public void setPhoneNumber(String phoneNumber) {this.phoneNumber = phoneNumber;}
}